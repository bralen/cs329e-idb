#-----------------------------------------
# main.py
# creating first flask application
#-----------------------------------------
#
#
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from create_db import create_books
from models import app, db, Book, Author, Publisher
import os
# create a flask object (flask needs an object to represent the application)
# app = Flask(__name__)
# create_books()
# The route decorator '@app.route()' maps a function to a route on your website.
# decorators are used to map a function, index(), to a web page, / or
# i.e., when someone types in the home address of the web site,
# flask will run the function index()
# summary: type in a URL, flask check the URL, finds the associate function with it, runs the
# function, collect responses, and send back the results to the browser.
@app.route('/')
def index():
    return render_template('home.html')

@app.route('/about/')
def about():
    return render_template('about.html')

@app.route('/books/')
def books():
    books = db.session.query(Book).all()
    return render_template('books.html', books = books)

@app.route('/publishers/')
def publishers():
    publishers = db.session.query(Publisher).all()
    publist = []
    for i in publishers:
        contains = False
        for j in publist:
            if i.p_name == j.p_name:
                contains = True
        if contains:
            pass
        else:
            publist.append(i)          
    return render_template('publishers.html', publishers = publist)

@app.route('/authors/')
def authors(): 
    authors = db.session.query(Author).all()
    authlist = []
    for i in authors:
        contains = False
        for j in authlist:
            if i.a_name == j.a_name:
                contains = True
        if contains:
            pass
        else:
            authlist.append(i)  
    return render_template('authors.html', authors = authlist)

@app.route('/oneauthor/<authorid>')
def oneauthor(authorid):
    author = db.session.query(Author).filter_by(id = authorid).one()
    return render_template('oneauthor.html', author = author)

@app.route('/onepublisher/')
def onepublisher():
    return render_template('onepublisher.html', publishers = publishers)

@app.route('/onebook/')
def onebook():
    return render_template('onebook.html', books = books)


# if main.py is run directly, i.e., as the main module, it will be assigned the value main
# and if it's main go ahead and run the application.
# if this application is imported, then the __name__ is no longer __main__ and
# the code, app.run(), will not be executed
if __name__ == "__main__":
    app.debug = True
    app.run()
#----------------------------------------
# end of main.py
#-----------------------------------------

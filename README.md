# cs329e-idb

CS329e, Group 6
This repo creates a static website that displays information about books, authors and
publishers in a responsive way.

The json folder contains books.html and books.json, which are not implemented and will be used
in the second phase of the project.

The static folder contains the css folder and the images folder. The css folder contains a css style sheet.
The images folder conatins all the images we use on our website.

The templates folder contains the hmtl templates for all of our pages, and a few extra pages that are not
implemented.

The api.py file is not implemented, but will be used for api calls.

The main.py file contains the flask import and sets up the website structure.

You can run the application by cloning the directory, making sure you are in the main directory
and running the command "python3 main.py" or "python main.py". Locally this will provide you with a
link to the development server.

We will also deploy the website on GCP.
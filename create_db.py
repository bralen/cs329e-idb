# beginning of create_db.py
import json
from models import app, db, Book, Author, Publisher

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def create_books():
    book = load_json('books.json')
    count = 0
    for oneBook in book['books']:

        a_id = count
        a_name = oneBook["authors"][0]["name"]
        try:
            date_of_birth = oneBook["authors"][0]["born"]
        except KeyError:
            date_of_birth = "N/A"
        try:
            nationality = oneBook["authors"][0]["nationality"]
        except KeyError:
            nationality = "N/A"
        try:
            a_website = oneBook["authors"][0]["wikipedia_url"]
        except KeyError:
            a_website = "N/A"
        try:
            a_image = oneBook["authors"][0]["image_url"]
        except:
            a_image = "N/A"

        newAuthor = Author(a_id = a_id, a_name = a_name, date_of_birth = date_of_birth, nationality = nationality, a_website = a_website, a_image = a_image)
        
        db.session.add(newAuthor)
        db.session.commit()

        p_id = count
        p_name = oneBook["publishers"][0]["name"]
        try:
            founded = oneBook["publishers"][0]["founded"]
        except KeyError:
            founded = "N/A"
        try:
            p_website = oneBook["publishers"][0]["website"]
        except KeyError:
            p_website = "N/A"
        try:
            description = oneBook["publishers"][0]["description"]
        except KeyError:
            description = "N/A"
        try:
            p_image = oneBook["publishers"][0]["image_url"]
        except KeyError:
            p_image = "N/A"

        newPublisher = Publisher(p_id = p_id, p_name = p_name, founded = founded, p_website = p_website, description = description, p_image = p_image)

        db.session.add(newPublisher)
        db.session.commit()
        
        title = oneBook['title']
        id = count
        publication_date = oneBook["publication_date"]
        author = oneBook["authors"][0]["name"]
        publisher = oneBook["publishers"][0]["name"]
        cover = oneBook["image_url"]
		
        newBook = Book(title = title, id = id, publication_date = publication_date, author = author, publisher = publisher, cover = cover)
        
        # After I create the book, I can then add it to my session. 
        db.session.add(newBook)
        # commit the session to my DB.
        db.session.commit()
        count += 1
		
create_books()
# end of create_db.py
